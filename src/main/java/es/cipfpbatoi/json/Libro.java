package es.cipfpbatoi.json;

import com.google.gson.annotations.SerializedName;

public class Libro {

    @SerializedName(value="Id", alternate = "Id del libro")
    private int id;

    @SerializedName(value="Título", alternate = "Título del libro")
    private String titulo;

    @SerializedName(value="Autor", alternate = "AUtor del libro")
    private String autor;

    @SerializedName(value="Editorial", alternate = "Editorial del libro")
    private String editor;

    @SerializedName(value="ISBN", alternate = "ISBN del libro")
    private String isbn;

    @SerializedName(value="Páginas", alternate = "Número de páginas del libro")
    private short paginas;

    public Libro() {        
    }
    
    public Libro(int id, String titulo, String autor, String editor, String isbn, short paginas) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.editor = editor;
        this.isbn = isbn;
        this.paginas = paginas;
    }
    
   public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }
    
    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditor() {
        return editor;
    }
    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public short getPaginas() {
        return paginas;
    }
    public void setPaginas(short paginas) {
        this.paginas = paginas;
    }

    @Override
    public String toString() {
        return "Libro{" + "nombre=" + titulo + ", autor=" + autor + ", editor=" + editor + ", isbn=" + isbn + ", paginas=" + paginas + '}';
    }   

}
