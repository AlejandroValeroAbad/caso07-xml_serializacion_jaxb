package es.cipfpbatoi.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Catalogo {

    @SerializedName(value="Bibliotecas", alternate = "Lista de Bibliotecas")
    private List<es.cipfpbatoi.json.Biblioteca> listaBiblioteca;

    public Catalogo(){

    }
    public List<Biblioteca> getListaBiblioteca() {
        return listaBiblioteca;
    }
    public void setListaBiblioteca(List<Biblioteca> ll) {
        this.listaBiblioteca = ll;
    }
}
