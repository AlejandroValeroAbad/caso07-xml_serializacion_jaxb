package es.cipfpbatoi.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Biblioteca {

    @SerializedName(value="Nombre", alternate = "Nombre de la biblioteca")
    private String nombre;

    @SerializedName(value="Ubicación", alternate = "Ubicación de la biblioteca")
    private String ubicacion;

    @SerializedName(value="Libros", alternate = "Lista de libros")
    private List<Libro> listaLibros;    

    public Biblioteca() {
    }    

    public List<Libro> getListaLibros() {
        return listaLibros;
    }
    public void setListaLibros(List<Libro> bl) {
        this.listaLibros = bl;
    }    

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
