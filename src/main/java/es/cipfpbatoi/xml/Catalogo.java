package es.cipfpbatoi.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


@XmlRootElement(name = "bibliotecas", namespace = "biblios")
public class Catalogo {

    private List<Biblioteca> listaBiblioteca;

    public Catalogo(){

    }
    @XmlElementWrapper(name = "catalogo")
    @XmlElement(name = "biblioteca")
    public List<Biblioteca> getListaBiblioteca() {
        return listaBiblioteca;
    }
    public void setListaBiblioteca(List<Biblioteca> ll) {
        this.listaBiblioteca = ll;
    }
}
