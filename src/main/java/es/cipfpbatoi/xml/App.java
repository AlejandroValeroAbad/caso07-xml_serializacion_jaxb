package es.cipfpbatoi.xml;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class App {

    private static final String BIBLIOTECA_XML = "biblio-jaxb.xml";

    public static void main(String[] args) {

        ArrayList<Libro> listaLibros1 = new ArrayList<Libro>();

        // Creamos los libros
        Libro libro1 = new Libro();
        libro1.setId(1);
        libro1.setIsbn("978-0060554736");
        libro1.setTitulo("The Game");
        libro1.setAutor("Neil Strauss");
        libro1.setEditor("Harpercollins");
        libro1.setPaginas((short) 200);
        listaLibros1.add(libro1);

        Libro libro2 = new Libro();
        libro2.setId(2);
        libro2.setIsbn("978-3832180577");
        libro2.setTitulo("Feuchtgebiete");
        libro2.setAutor("Charlotte Roche");
        libro2.setEditor("Dumont Buchverlag");
        libro2.setPaginas((short) 100);
        listaLibros1.add(libro2);
        
        Libro libro3 = new Libro(3, "Otro libro", "Autor X", "Editor X", "123-412343423", (short) 100);
        listaLibros1.add(libro3);

        // Creamos la biblioteca y le asignamos los libros
        Biblioteca biblioteca1 = new Biblioteca();
        biblioteca1.setNombre("Fraport Bookstore");
        biblioteca1.setUbicacion("Frankfurt Airport");
        biblioteca1.setListaLibros(listaLibros1);

        Biblioteca biblioteca2 = new Biblioteca();
        biblioteca2.setNombre("Batoi");
        biblioteca2.setUbicacion("Alcoi");
        biblioteca2.setListaLibros(listaLibros1);

        List<Biblioteca> bibliotecas = new ArrayList<>();
        bibliotecas.add(biblioteca1);
        bibliotecas.add(biblioteca2);

        Catalogo catalogo = new es.cipfpbatoi.xml.Catalogo();
        catalogo.setListaBiblioteca(bibliotecas);

        try {

            // Creamos el contexto JAXB basado en Biblioteca.class
            JAXBContext context = JAXBContext.newInstance(Catalogo.class);

            // Instanciamos un marshaller o serializador
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");               // valor por defecto

            m.marshal(catalogo, System.out);

            // Escritura del fichero XML 
            try (FileWriter fw = new FileWriter(BIBLIOTECA_XML)) {
                m.marshal(catalogo, fw);
            } catch (IOException ex) {
                System.err.println("Problemas durante la escritura del fichero..." + ex.getMessage());
            }

            // Lectura del fichero XML            
            Unmarshaller um = context.createUnmarshaller();            
            Catalogo catalogo0 = new Catalogo();
            try {
                catalogo0 = (Catalogo) um.unmarshal(new FileReader(BIBLIOTECA_XML));
            } catch (FileNotFoundException ex) {
                System.err.println("Parece que el fichero no existe..." + ex.getMessage());
            }

            if (catalogo0 != null) {

                List<Biblioteca> listaBibliotecas = catalogo0.getListaBiblioteca();

                for (Biblioteca b:listaBibliotecas) {
                    List<Libro> listaLibros2 = b.getListaLibros();
                    System.out.println("Nombre: " + b.getNombre());
                    System.out.println("Ubicacion: " + b.getUbicacion());
//                for (Libro libro : listaLibros2) {
//                    System.out.println(libro);
//                }
                    listaLibros2.stream().forEach(System.out::println);
                }
            }

        } catch (JAXBException ex) {
            System.out.println("Problemas durante la lectura/escritura del archivo xml " + BIBLIOTECA_XML + " " + ex.getMessage());
        }

    }
}


